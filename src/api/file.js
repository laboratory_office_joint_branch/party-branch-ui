import { axios } from '@/utils/request'

// 用户登录
export function Userlogin (params) {
  var form = new FormData()
  form.append('userId', params.userId)
  form.append('password', params.password)
  return axios({
    url: '/api/login',
    method: 'post',
    data: form,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 查找用户
export function findByUserId (params) {
  return axios({
    url: '/api/findByUserId',
    method: 'get',
    params: {
      'userId': params.userId
    }
  })
}

// 删除用户
export function deleteByUserId (params) {
  return axios({
    url: '/api/deleteByUserId',
    method: 'delete',
    params: {
      'userId': params.userId
    }
  })
}

export function deleteUserList (params) {
  return axios({
    url: '/api/deleteUserList',
    method: 'delete',
    params: {
      'userIdList': params.userIdList
    }
  })
}

// 更改密码
export function editByUserId (params) {
  return axios({
    url: '/api/editByUserId',
    method: 'post',
    params: {
      'userId': params.userId,
      'passwordUpdated': params.passwordUpdated,
      'passwordConfirmed': params.passwordConfirmed
    }
  })
}

// 创建文章(有封面、无封面)
export function createWithCover (params) {
  var form = new FormData()
  form.append('articleTitle', params.articleTitle)
  form.append('articleTag', params.articleTag)
  form.append('articleType', params.articleType)
  form.append('content', params.content)
  form.append('img', params.img)
  return axios({
    url: '/api/createWithCover',
    method: 'post',
    data: form,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

export function createWithoutCover (params) {
  var form = new FormData()
  form.append('articleTitle', params.articleTitle)
  form.append('articleTag', params.articleTag)
  form.append('articleType', params.articleType)
  form.append('content', params.content)
  return axios({
    url: '/api/createWithoutCover',
    method: 'post',
    data: form,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 查询文章
export function getArticleContent (params) {
  return axios({
    url: '/api/getArticleContent',
    method: 'get',
    params: {
      'articleId': params.articleId
    }
  })
}

export function findAllArticle () {
  return axios({
    url: '/api/findAllArticle',
    method: 'get'
  })
}

export function findAllUser () {
  return axios({
    url: '/api/findAllUser',
    method: 'get'
  })
}

export function findByArticleId (params) {
  return axios({
    url: '/api/findByArticleId',
    method: 'get',
    params: {
      'articleId': params.articleId
    }
  })
}

export function findByArticleTitle (params) {
  return axios({
    url: '/api/findByArticleTitle',
    method: 'get',
    params: {
      'articleTitle': params.articleTitle
    }
  })
}

// 编辑文章
export function editByArticleId (params) {
  return axios({
    url: '/api/editByArticleId',
    method: 'post',
    params: {
      'articleId': params.articleId,
      'articleTitle': params.articleTitle,
      'content': params.content
    }
  })
}

// 删除文章
export function deleteByArticleId (params) {
  return axios({
    url: '/api/deleteByArticleId',
    method: 'delete',
    params: {
      'articleId': params.articleId
    }
  })
}

export function deleteArticleList (params) {
  var form = new FormData()
  form.append('articleIdList', params.articleIdList)
  return axios({
    url: '/api/deleteArticleList',
    method: 'post',
    data: form,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}
