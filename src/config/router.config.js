// eslint-disable-next-line
import { UserLayout, BasicLayout, RouteView, BlankLayout, PageView } from '@/layouts'
/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [
  {
    path: '/',
    name: 'index',
    component: () => import('@/views/index'),
    meta: {
      title: '主页',
      requireLogin: true
    }
  },
  {
    path: '/desc',
    name: 'desc',
    component: () => import('@/views/desc'),
    meta: { title: '支部概况' }
  },
  {
    path: '/job',
    name: 'job',
    component: () => import('@/views/job'),
    meta: { title: '工作动态' },
    props: true
  },
  {
    path: '/build',
    name: 'build',
    component: () => import('@/views/build'),
    meta: { title: '思想建设' }
  },
  {
    path: '/study',
    name: 'study',
    component: () => import('@/views/study'),
    meta: { title: '学习资料' }
  },
  {
    path: '/view',
    name: 'view',
    component: () => import('@/views/view'),
    meta: { title: '文章展示' }
  },
  {
    path: '/nav',
    name: 'nav',
    component: () => import('@/views/nav'),
    meta: { title: '导航栏' },
    props: true
  },
  {
    path: '/back',
    name: 'Back',
    component: BasicLayout,
    redirect: '/back/user',
    children: [
      {
        path: 'user',
        name: 'BackUser',
        component: () => import('@/views/back/user/user'),
        meta: { title: '主页', icon: 'user', keepAlive: true }
      },
      {
        path: '/back/article',
        name: 'BackArticle',
        redirect: '/back/article/list',
        component: RouteView,
        meta: { title: '文章管理', icon: 'user' },
        children: [
          {
            path: 'list',
            name: 'BackArticleList',
            component: () => import('@/views/back/article/list'),
            meta: { title: '文章列表' }
          },
          {
            path: 'new',
            name: 'BackArticleNew',
            component: () => import('@/views/back/article/new'),
            meta: { title: '创建文章', keepAlive: true }
          }
        ]
      }
    ]
  }
]
