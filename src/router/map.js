/* eslint-disable eol-last */
/**
 * 路由名单
 */
export const whiteList = ['index', 'desc', 'job', 'build', 'study', 'view', 'nav']
export const adminList = ['Back', 'BackUser', 'BackArticle', 'BackArticleList', 'BackArticleNew', 'AdminSystemRole', 'AdminSystemPermission', 'AdminUser', 'AdminUserStudent', 'AdminUserTeacher', 'AdminTeach', 'AdminTeachTrain', 'AdminTeachTrainCourse', 'AdminTeachPlan', 'AdminTeachPlanNew', 'AdminTeachPlanStudent', 'AdminEnvironment', 'AdminImage', 'BackAccount']
